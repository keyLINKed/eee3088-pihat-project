EEE3088 PiHat Project

UPS PiHat project for EEE3088

Group Members:
Matthew Gruning
Lauren McKenzie
Phenyo Dantjie

This UPS design is made for household use.

The UPS is connected to the alternating current supplied by the house main. Thus, for it to be used, the mains power
must first pass through a 230-12V transformer. Our UPS is also connected to a battery for when the mains power is down and
automatically swaps to the battery source. There is also a maintenace switch on our UPS should and testing need to 
be done. Once this is completed the switch must be closed and the UPS must be connected to mains again.

Should anyone wish to modify this any input will be appreciated and revised before changes are made. This project
can be used for your own personal use, but may not be used for commercial benefits.
